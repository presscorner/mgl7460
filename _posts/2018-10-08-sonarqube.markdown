---
layout: post
title:  "SonarQube & Sonarcloud"
date:   2020-02-20 03:00:00 -0500
categories: mgl7460 sonarqube java gradle intellij
---

1. A markdown unordered list which will be replaced with the ToC, excluding the "Contents header" from above
{:toc}

## Introduction


Ce laboratoire se veut une exploration de [SonarQube][sonarqube], un outil d'inspection continue de la qualité du code.

Afin d'éviter les [étapes d'installation du serveur SonarQube][sonarqube-server], le laboratoire utilisera le service [Sonarcloud][sonarcloud-about].

### Prérequis

1. Une copie à jour du dépôt [labo-tdd][labo-tdd]
2. Émulateur de terminal
3. Un compte GitHub, Bitbucket ou VSTS (pour créer et accéder son compte Sonarcloud)
3. (optionnel) Une installation de IntelliJ IDEA

## Sonarcloud

1. Créer un compte Sonarcloud en visitant l'adresse https://sonarcloud.io/sessions/new

    Sonarcloud offre l'authorisation exclusivement via GitHub, Bitbucket ou VSTS. Il vous faut donc un compte sur l'un ou l'autre de ces services.

2. Créer un projet en assignant un project name, project key ainsi qu'un token d'authentification depuis l'interface web de création de projet manuel de Sonarcloud [(Create new project or organization > Create new project > Create manually)][https://sonarcloud.io/projects/create?manual=true]. Sonarcloud demande ensuite de choisir le langage de programmation du projet (java) et votre outil de build (gradle) puis donne les ajouts à faire à la configuration et la commande à executer pour lancer une analyse SonarQube.

## Configuration de Gradle pour l'utilisation de SonarQube

1. Modifié le fichier `gradle.build` de sorte que

    <script src="https://gitlab.com/yakoi/mgl7460/snippets/1761892.js"></script>

2. Lancer `gradle sonarqube` en renseignant la commande fournie par Sonarcloud à la section précédente :

    ```console
    $ gradle sonarqube \
      -Dsonar.projectKey=VOTRE-PROJECT-KEY \
      -Dsonar.organization=VOTRE-ORGANISATION \
      -Dsonar.host.url=https://sonarcloud.io \
      -Dsonar.login=VOTRE-TOKEN
    ```

3. Consulter le rapport de l'analyse sur l'interface web de Sonarcloud

## SonarQube `Quality Gate`

La porte qualité (Quality gate) est un indicateur binaire (pass / fail) de la qualité du projet selon certains critères paramètrés. À chaque analyse, la porte qualité échoue si un ou l'autre des critères échoue

Par défaut, SonarQube a une `Quality Gate` par défaut configurée tel que :

* Couverture >= `80 %`
* Pourcentage de lignes de code dupliquées <= `3`
* Le classement  pour l'un ou l'autre des volets `maintainability`, `reliability `ou `security ` < `A`

Avec un comte administrateur sur une installation personalisée de SonarQube, il est possible (et reccomandé) d'adapter ces critères

## Installation du plugin IntelliJ IDEA Sonarlint (optionnel)

1. Télécharger le plugin Sonarlint depuis la [page du plugin][https://plugins.jetbrains.com/plugin/7973-sonarlint]
2. Installer le plugin dans IntelliJ

    Dans les configurations des plugins (File > Settings > Plugins) Choisir `Install plugin from disk` et sélectionner le fichier zip téléchargé à l'étape précédente.

3. Redémarrer IntelliJ IDEA

## Intégration Gitlab CI <> Sonarcloud

1. [Ajouter une variable Gitlab SONAR_TOKEN](https://gitlab.com/help/ci/variables/README#limiting-environment-scopes-of-secret-variables)

2. Ajouter un fichier de configuration Gitlab CI `.gitlab-ci.yml`

    Ajouter un nouveau fichier avec [le contenu suivant](https://gitlab.com/yakoi/labo-tdd/blob/06ab3828f31ccd3962d7c0d743b20a4c0b22c79f/.gitlab-ci.yml) en y injectant vos valeurs (projectKey, sonar.organization).

    Noter la référence à la variable SONAR_TOKEN.

Tout commit sur master déclanchera maintenant une nouvelle analyse SonarQube qui sera envoyée à Sonarcloud.


[tdd-lab]: http://www.micsymposium.org/mics_2005/papers/paper10.pdf
[labo-tdd]: https://gitlab.com/yakoi/labo-tdd
[sonarcloud-about]: https://sonarcloud.io/about/sq
[sonarqube]: https://www.sonarqube.org/
[sonarqube-2min]: https://docs.sonarqube.org/display/SONAR/Get+Started+in+Two+Minutes
[sonarqube-server]: https://docs.sonarqube.org/display/SONAR/Installing+the+Server
